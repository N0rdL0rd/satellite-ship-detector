FROM python:3.10

WORKDIR /app

COPY . .



RUN git clone https://github.com/ultralytics/yolov5


RUN pip install -r yolov5/requirements.txt
RUN pip install flask opencv-python-headless


CMD [ "python", "-m" , "flask", "run", "--host=0.0.0.0"]