from flask import Flask, render_template, json, request
from yolov5.detect import run
from PIL import Image
import base64
import io
import re
import os

app = Flask(__name__)

@app.get('/')
@app.get('/index')
def index():
    return render_template('index.html')


# @app.route('/')
# @app.route('/index')
@app.post('/send_image')
def send_image():

    img = request.json['style']
    image = Image.open(io.BytesIO(base64.b64decode(re.sub(".*base64", '', img)))).convert('RGB')
    print(image.size)
    image.save('my-image.png')

    try:
        os.mkdir("out")
    except:
        pass

    run(weights='best.pt',
        source='my-image.png',
        imgsz=image.size,
        conf_thres=0.6,
        # device='0',
        project='out',
        name='',
        exist_ok=True)

    with open("out"+os.sep+"my-image.png", "rb") as image_file:
        data = base64.b64encode(image_file.read()).decode("utf-8")

    return json.dumps(obj={"src": f"data:image/png;base64,{data}"})

if __name__ == '__main__':
    app.run()