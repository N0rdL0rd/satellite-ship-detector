/*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */
function readURL(input, type) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult' + type)
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function proc(){
    let p = document.getElementById("status");
    p.innerHTML = "<p style='font-size:4em;'>Processing...</p>";
}

function dn(){
    let p = document.getElementById("status");
    p.innerHTML = "<p style='font-size:4em;'>Done</p>";
}

function postImage() {
    let imageStyle = document.getElementById("imageResultStyle");

    $.ajax({
        type: 'POST',
        url: '/send_image',
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        data: JSON.stringify({
            'style': imageStyle.src,
        })
    }).done(function(response) {

        $("#imageResultStyle").attr("src", response.src);
        dn();
    }).fail((e) => {
        console.log("Fail!")
    });
}

$(function () {
    $('#uploadStyle').on('change', function () {
        readURL(input);
    });
});

/*  ==========================================
    SHOW UPLOADED IMAGE NAME
* ========================================== */
var input = document.getElementById( 'upload' );
var infoArea = document.getElementById( 'upload-label' );

input.addEventListener( 'change', showFileName );
function showFileName( event ) {
  var input = event.srcElement;
  var fileName = input.files[0].name;
  infoArea.textContent = 'File name: ' + fileName;
}